'use strict';

const video = document.querySelector('.presentation_video iframe');
const videoBlock = document.querySelector('.presentation_video');
const playBtn = document.querySelector('.poster--play');
let url;


setVideoUrl(video);


playBtn.addEventListener('click', (e) => {
    e.preventDefault();
    playVideo(video);
});



function setVideoUrl(item) {
    url = item.getAttribute('data-url');
    video.setAttribute('src', `https://www.youtube-nocookie.com/embed/${url}?enablejsapi=1&rel=0&html5=1&controls=1&showinfo=0&autoplay=0`)
}

function playVideo(item) {
    url = item.getAttribute('data-url');
    video.setAttribute('src', `https://www.youtube-nocookie.com/embed/${url}?enablejsapi=1&rel=0&html5=1&controls=1&showinfo=0&autoplay=1`);
    videoBlock.classList.add('play');
}
